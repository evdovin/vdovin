import RealmSwift
import SwiftyJSON

class ScheduleModel: Object, Comparable {
    @objc dynamic var id = ""
    @objc dynamic var name = ""
    @objc dynamic var desc = ""
    @objc dynamic var teacher = ""
    @objc dynamic var place = ""
    @objc dynamic var startTime = ""
    @objc dynamic var endTime = ""
    @objc dynamic var weekDayInt = 0

    var weekDay: String {
        switch weekDayInt {
        case 1: return "Понедельник"
        case 2: return "Вторник"
        case 3: return "Среда"
        case 4: return "Четверг"
        case 5: return "Пятница"
        case 6: return "Суббота"
        case 7: return "Воскресенье"
        default: return ""
        }
    }
    
    convenience init(from json: JSON) {
        self.init()
        self.id = json["appointment_id"].stringValue
        self.name = json["name"].stringValue
        self.desc = json["description"].stringValue
        self.teacher = json["teacher"].stringValue
        self.place = json["place"].stringValue
        self.startTime = json["startTime"].stringValue
        self.endTime = json["endTime"].stringValue
        self.weekDayInt = json["weekDay"].intValue
    }
    
    override class func primaryKey() -> String? {
        "id"
    }

    override class func ignoredProperties() -> [String] {
        ["weekDay"]
    }
    
    static func < (lhs: ScheduleModel, rhs: ScheduleModel) -> Bool {
        lhs.startTime < rhs.startTime
    }
}
