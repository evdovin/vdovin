import RealmSwift
import SwiftyJSON

class Storage {
    func saveObjects(from data: Data) {
        let json = JSON(data)
        print(json)
        do {
            let realm = try Realm()
            try realm.write {
                let models = json.arrayValue.map {
                    ScheduleModel(from: $0)
                }
                for model in models {
                    realm.add(model, update: .all)
                }
            }
        } catch let error {
            print(error)
        }
    }

    func storageIsEmpty() -> Bool {
        do {
            let realm = try Realm()
            return realm.objects(ScheduleModel.self).isEmpty
        } catch let error {
            print (error)
            return true
        }
    }

    func getObjects<T: Object>() -> Results<T>? {
        do {
            let realm = try Realm()
            return realm.objects(T.self)
        } catch let error {
            print (error)
            return nil
        }
    }
}
