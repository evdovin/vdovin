//
//  TimeTableViewCell.swift
//  Vdovin
//
//  Created by Egor Vdovin on 26.02.2020.
//  Copyright © 2020 Egor Vdovin. All rights reserved.
//

import UIKit

class TimeTableViewCell: UITableViewCell {

    static let identifier = "TimeTableViewCell"

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var teacherLabel: UILabel!
    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var endTimeLabel: UILabel!
    @IBOutlet weak var placeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    func configure(from model: ScheduleModel) {
        self.nameLabel.text = model.name
        self.teacherLabel.text = model.teacher
        self.startTimeLabel.text = model.startTime
        self.endTimeLabel.text = model.endTime
        self.placeLabel.text = model.place
        self.descriptionLabel.text = model.desc
    }

    override func didMoveToSuperview() {
        layoutIfNeeded()
    }
}
