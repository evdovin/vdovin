import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var startButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkStorage()
        getData()
    }
    
    private func checkStorage() {
        if Storage().storageIsEmpty() {
            startButton.isHidden = true
            activityIndicator.startAnimating()
        }
    }

    private func getData() {
        Router().getRequest { [unowned self] completion in
            if completion {
                self.dataWasReceived()
            } else {
                self.showError()
            }
        }
    }

    private func dataWasReceived() {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
            self.startButton.isHidden = false
        }
    }

    private func showError() {
        let alert = UIAlertController(title: "Ошибка", message: "Неизвестная ошибка", preferredStyle: .alert)
        let action = UIAlertAction(title: "Ок", style: .cancel, handler: nil)
        
        alert.addAction(action)
        present(alert, animated: true, completion: { exit(0) })
    }
}

