import RealmSwift
import UIKit

class TimeTableViewController: UITableViewController {
    
    var scheduledModels: Results<ScheduleModel>?

    var days: [Int] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        commonInit()
    }

    private func commonInit() {
        tableView.tableFooterView = UIView()
        
        scheduledModels = Storage().getObjects()

        guard let scheduledModels = scheduledModels else { return }
        days = Array(Set(scheduledModels.compactMap { $0.weekDayInt })).sorted()
    }

    private func getModels(at section: Int) -> [ScheduleModel] {
        guard let scheduledModels = scheduledModels else { return [] }
        
        let models = scheduledModels.filter { [unowned self] in
            $0.weekDayInt == self.days[section]
        }.sorted()
        
        return models
    }
}

    // MARK: - Table view data source
extension TimeTableViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        days.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        getModels(at: section).count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TimeTableViewCell.identifier,
                                                 for: indexPath) as! TimeTableViewCell
        
        let models = getModels(at: indexPath.section)
        cell.configure(from: models[indexPath.row])

        return cell
    }

}

extension TimeTableViewController {
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        getModels(at: section).first?.weekDay ?? ""
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        50
    }
}
