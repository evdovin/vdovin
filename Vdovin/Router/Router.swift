import Foundation

class Router {
    let urlString = "https://sample.fitnesskit-admin.ru/schedule/get_group_lessons_v2/1/"
    
    func getRequest(completion: @escaping(_ isReady: Bool) -> Void) {
        guard let url = URL(string: urlString) else { return }
        let request = URLRequest(url: url)
        
        let task = URLSession.shared.dataTask(with: request) { data, _ , error in
            guard error == nil else {
                print (error.debugDescription)
                return
            }
            guard let data = data else { return }
            Storage().saveObjects(from: data)
            completion(true)
        }
        task.resume()
    }
}
